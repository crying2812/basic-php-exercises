<?php
/**
 *   Create a function to create a new string from a given string without the first and last character
 * if the first or last characters are 'a' otherwise return the original given string
 * Sample Output:
 * "aTuan"
 * "aKaiyouIT"
 * "Japan"
 * Expected output:
 * "Tuan"
 * "KaiyouIT"
 * "Japan"
 */

