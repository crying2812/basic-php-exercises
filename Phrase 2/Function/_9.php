<?php
/**
 *   Create a function to create a new array of given length using the odd numbers from a given array of positive integers
 * Sample Input:
{1,2,3,5,7,9,10},3
Expected Output:
New array: 1,3,5
 */

